<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ApiRequest extends FormRequest
{
    public function wantsJson()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->wantsJson()) {
            // flatten all the message
            $collection  = collect($validator->errors())->all();
            throw new HttpResponseException(response()->json(["message" => "The given data was invalid.", 'errors' => $collection]));
        }

        parent::failedValidation($validator);
    }
}
