<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Trip\TripCreateRequest;
use App\Http\Resources\Trip as TripResource;
use App\Http\Resources\TripCollection as TripCollectionResource;

use App\Models\Trip;

class TripController extends Controller
{
    public function index(Request $request)
    {
        $zoneId = $request->get('zone');

        if ( is_null($zoneId) ) {
            return response()->json([
                'status' => 400,
                'message' => __('Missing required "zone" parameter')
            ]);
        }

        $trips = Trip::where('zone_id', $zoneId)->get();

        return new TripCollectionResource( $trips );

    }

    /**
     * @param TripCreateRequest $request
     *
     * @return TripResource
     */
    public function store(TripCreateRequest $request)
    {
        $trip = Trip::create(
            $request->validated()
        );

        return new TripResource( $trip );
    }
}
