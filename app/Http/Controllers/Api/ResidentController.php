<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;

use App\Models\Resident;

use App\Http\Resources\Resident as ResidentResource;
use App\Http\Resources\ResidentCollection as ResidentCollectionResource;

class ResidentController extends Controller
{
    /**
     * @param Request $request
     *
     * @return ResidentCollectionResource|JsonResponse
     */
    public function index(Request $request)
    {
        $township = $request->get('township');

        if ( is_null($township) ) {
            return response()->json([
                'status' => 400,
                'message' => __('Missing required "township" parameter')
            ]);
        }

        $residents = Resident::where('township', $township)->get();

        return new ResidentCollectionResource( $residents );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ResidentResource();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
