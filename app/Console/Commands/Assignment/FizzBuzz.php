<?php

namespace App\Console\Commands\Assignment;

use Illuminate\Console\Command;

class FizzBuzz extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignment:fizzbuzz';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simple FizzBuzz script showing multiplications of 3 and 5';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        # Simple loop till 100
        for ($i = 1; $i <= 100; $i++) {
            if ($i % 15 === 0) { # Number is a multiplication of 3 and 5
                $this->info("FizzBuzz");
            }
            else if($i % 3 == 0) { # Number is a multiplication of 3
                $this->info("Fizz");
            }
            else if($i % 5 == 0) { # Number is a multiplication of 5
                $this->info("Buzz");
            }
            else { # Number is a multiplication of 3 nor 5
                $this->info($i);
            }
        }
    }
}
