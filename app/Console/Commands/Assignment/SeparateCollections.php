<?php

namespace App\Console\Commands\Assignment;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class SeparateCollections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignment:separate:collections {list?} {--delimiter=,} {--collection-size=4}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert a given list into a list of smaller lists';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        # Get a given list
        $givenList = $this->argument('list');

        # Set the collection size, default = 4
        $collectionSize = $this->option('collection-size');

        # Set the delimiter to explode the given list, default = ','
        $delimiter = $this->option('delimiter');

        # Check if a list is given
        if ( !is_null($givenList) ) {
            # Generate a list of numbers to separate in separate collections
            $list = $this->generateNumberList();
        } else {
            # Explode the given list by the provided delimiter
            $list = explode($delimiter, $givenList);
        }

        # Separate the list into a collection of separate collections
        $separatedCollections = $this->separateListIntoCollections($list, $collectionSize);

        # Dump the result
        dd($separatedCollections);
    }

    /**
     * Separate a provided lise in batches of a given size
     *
     * @param $list
     * @param $collectionSize
     *
     * @return Collection
     */
    protected function separateListIntoCollections($list, $collectionSize) : Collection
    {
        # Collection to hold all sub collections
        $collectionOfCollections = collect();
        # Sub collection to store batches of provided list
        $subCollection = collect();

        $iterator = 1;

        # Loop over the list and add batches of the given collectionSize to the collectionOfCollections
        foreach ($list as $index => $item) {
            $subCollection->add($item);

            # Add the batch to the main collection, reset the subCollection iterator
            if ( $iterator >= $collectionSize ) {
                $collectionOfCollections->add($subCollection);
                $subCollection = collect();
                $iterator = 1;
            } else { # Increment the iterator until collectionSize is met
                $iterator++;
            }
        }

        return $collectionOfCollections;
    }

    /**
     * Generate a list of numbers for sample use
     *
     * @return Collection
     */
    protected function generateNumberList() : Collection
    {
        $numList = collect();

        for ($i = 1; $i <= 100; $i++) {
            $numList->push($i);
        }

        return $numList;
    }
}
