<?php

namespace App\Console\Commands\Assignment;

use Exception;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class Mondays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignment:mondays {start-date} {end-date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all week numbers and Mondays of full weeks between given dates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        # Get the given start date
        $startDate = $this->argument('start-date');
        # Get the given end date
        $endDate = $this->argument('end-date');

        # Check if two valid days were given
        if (
            ($startDate = $this->dateIsValidCarbonDate($startDate)) &&
            ($endDate = $this->dateIsValidCarbonDate($endDate))
        ) {
            $weekDayInfo = $this->getAllMondaysOfFullWeeksBetween($startDate, $endDate);

            # Dump the result
            dd($weekDayInfo);
        }
    }

    /**
     * @param $date
     *
     * @return Carbon|bool
     */
    protected function dateIsValidCarbonDate( $date )
    {
        $validDate = false;

        try {
            $validDate = Carbon::parse($date);
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }

        return $validDate;
    }

    /**
     * @param Carbon $startDate
     * @param Carbon $endDate
     *
     * @return array
     */
    protected function getAllMondaysOfFullWeeksBetween(Carbon $startDate, Carbon $endDate) : array
    {
        $weekDayInfo = [];

        # Loop until the start date is no longer smaller or equal to the end date
        while ($startDate <= $endDate) {
            # Get the day in 0-6 (Sunday - Saturday) format
            $day = $startDate->isoFormat('d');

            # Check if day is 1 (Monday) and there are more or equal to 6 days left in the week
            if ($day == 1 && $startDate->diffInDays($endDate) >= 6 ) {
                # Add the week number of full weeks to the array, together with date and day for extra verification
                $weekDayInfo[] = [
                    'week_nr' => $startDate->isoFormat('W'),
                    'date' => $startDate->toDateString(),
                    'day' => $startDate->isoFormat('dddd')
                ];
            }

            # Increment the day
            $startDate->addDay();
        }

        return $weekDayInfo;
    }
}
