<?php

namespace App\Console\Commands\Beschikking\Budget;

use App\Models\Beschikking;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Renew extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'beschikking:budget:renew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset beschikking budget to initial defined budget';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        # Get all beschikkingen requiring renewal
        $beschikkingen = Beschikking::requiresRenewal()->get();

        $failedToRenew = 0;

        /** @var Beschikking $beschikking */
        foreach ($beschikkingen as $beschikking) {
            # Renew the beschikking
            $renewed = $beschikking->renew();

            # If failed to renew, log critical and count failed renewals
            if ( !$renewed ) {
                Log::critical(
                    __('Could not renew beschikking with id :id', [
                        'id' => $beschikking->id
                    ])
                );
                $failedToRenew++;
            }
        }

        Log::info(
            __('Successfully renewed :success_num beschikkingen. :fail_num failed to renew.', [
                'success_num' => $beschikkingen->count() - $failedToRenew,
                'fail_num' => $failedToRenew
            ])
        );
    }
}
