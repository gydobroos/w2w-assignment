<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resident extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'township',
        'beschikking_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function trips()
    {

    }

    public function beschikking()
    {
        return $this->hasOne( Beschikking::class, 'beschikking_id' );
    }
}
