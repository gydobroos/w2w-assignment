<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Beschikking extends Model
{
    protected $table = 'beschikkingen';

    protected $fillable = [
        'defined_budget',
        'budget',
        'renewed_at',
    ];

    /**
     * Renew beschikking by setting budget equal to defined_budget
     * and renewed_at to current datetime string
     *
     * @return bool
     */
    public function renew()
    {
        $updated = $this->update([
            'budget' => $this->defined_budget,
            'renewed_at' => Carbon::now()->toDateTimeString()
        ]);

        return $updated;
    }

    /**
     * Beschikkingen should be renewed after one year.
     *
     * @param $query
     */
    public function scopeRequiresRenewal( $query )
    {
        $dateTimeOneYearAgo = Carbon::now()->subYear();

        $query->where('renewed_at', '<=', $dateTimeOneYearAgo);
    }
}
