# About Assignment

## Relevant folders:

#### Assignment 1-3:
- App/Console/Commands/Assignment

##### Commands
###### Simple FizzBuzz script showing multiplications of 3 and 5
$ artisan assignment:fizzbuzz
###### Show all week numbers and Mondays of full weeks between given dates.
$ artisan assignment:mondays               
###### Convert a given list into a list of smaller lists
$ artisan assignment:separate:collections  


#### Assignment 4:
- App/Console/Commands
- App/Http/Controllers/Api
- App/Http/Requests
- App/Http/Resources
- App/Models
- database
##### API Requests
###### Get all residents within the township utrecht
- GET - /api/residents?township=utrecht
###### Create a trip for a resident within a certain zone
- POST - /api/trips {resident_id: int Resident, zone_id: int Zone}
###### Get all trips within a certain zone
- GET - /api/trips?zone={zone_id}

##### Commands
###### Renew budgets
$ artisan beschikking:budget:renew
