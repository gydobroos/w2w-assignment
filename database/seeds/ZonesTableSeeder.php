<?php

use App\Models\Zone;
use App\Models\Beschikking;
use Illuminate\Database\Seeder;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Zone $zone */
        factory(Zone::class, 100)->create();
    }
}
