<?php

use App\Models\Resident;
use App\Models\Beschikking;
use Illuminate\Database\Seeder;

class ResidentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Resident $resident */
        factory(Resident::class, 500)->create()->each(
            function($resident) {
                $beschikking = factory(Beschikking::class)->create();

                $resident->update(['beschikking_id' => $beschikking->id]);
            }
        );
    }
}
