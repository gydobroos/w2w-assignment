<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Beschikking;
use Faker\Generator as Faker;

$factory->define(Beschikking::class, function (Faker $faker) {
    return [
        'defined_budget' => $faker->randomFloat(2, 100, 10000),
        'budget' => $faker->randomFloat(2, 0, 6000),
        'renewed_at' => $faker->dateTime
    ];
});
