<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Resident;
use Faker\Generator as Faker;

$factory->define(Resident::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'township' => $faker->randomElement([
            'Amersfoort',
            'Baarn',
            'De Bilt',
            'Bunnik',
            'Bunschoten',
            'Eemnes',
            'Houten',
            'IJsselstein',
            'Leusden',
            'Lopik',
            'Montfoort',
            'Nieuwegein',
            'Oudewater',
            'Renswoude',
            'Rhenen',
            'De Ronde Venen',
            'Soest',
            'Vecht Stichtse Vecht',
            'Utrecht',
            'Heuvelrug Utrechtse Heuvelrug',
            'Veenendaal',
            'Vijfheerenlanden',
            'Wijk bij Duurstede',
            'Woerden',
            'Woudenberg',
            'Zeist',
        ]),
    ];
});
