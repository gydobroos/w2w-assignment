<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('beschikking_id')->unsigned()->nullable();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('township');

            $table->timestamps();
        });

        Schema::table('residents', function (Blueprint $table) {
            $table->foreign('beschikking_id')
                ->references('id')
                ->on('beschikkingen')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}
