<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('resident_id')->unsigned();
            $table->bigInteger('zone_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->foreign('resident_id')
                ->references('id')
                ->on('residents')
                ->onDelete('cascade');
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->foreign('zone_id')
                ->references('id')
                ->on('zones')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
